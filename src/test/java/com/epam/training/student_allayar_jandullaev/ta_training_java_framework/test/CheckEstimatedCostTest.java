package com.epam.training.student_allayar_jandullaev.ta_training_java_framework.test;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckEstimatedCostTest extends CommonConditions {
    @Test
    public void testCheckTotalEstimateCost() {
        //Assert should be assertTrue
        Assert.assertFalse(computeEnginePage.getTotalEstimatedCostIsVisible().contains("Total Estimated Cost: USD"),
                "Error message if the condition is not met");
    }
}
